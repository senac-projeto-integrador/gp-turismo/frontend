# Inicialização do Front-end
- Dados:
- [ ] Ajustar Get
- [ ] Ajustar Post
- [ ] Ajustar Put
- [ ] Ajustar Delete
-
- [ ] Criar Página na Dashboard para visualizar a Página Principal(E animar sidebar)
-
### Objetivo de cada Componente:
#### TripHistory:
- [ ] Listar todas viagens e excursões realizadas
- [ ] Destacar excursões com um ícone diferenciado 
- [ ] Quando for Excursão, mostrar um ícone em galeria para acessar as fotos da excursão.
- [ ] Mostrar apenas viagens/excursões com a 'Data Chegada < Data Atual'
-
#### TripCreate:
- [ ] Deve ser capaz de criar Viagens
- [ ] Deve ser capaz de escolher excursão ou viagem estudante.
- [ ] Quando escolher alguma opção, caso seja excursão, mostrar novas opções abaixo.
- [ ] Deve ser capaz de salvar a imagem carregada.
- [ ] Não pode permitir criar uma viagem para o dia anterior.
-
#### TripList:
- [ ] Deve ser capaz de listar todas as viagens que ainda não foram realizadas/pendentes.
- [ ] Deve permitir editar essas viagens.
- [ ] Deve ser capaz de excluir a viagem.
-
#### TripPoint:
- [ ] Deve ser capaz de selecionar uma viagem-estudante da trip-list.
- [ ] Deve ser capaz de adicionar várias paradas em uma pagina direcionada.
- [ ] Deve ser capaz de listar todas as paradas adicionadas.
- [ ] Deve ser capaz de excluir as paradas adicionadas.
-
#### Main.js
- [ ] Utilizar o Link principal de rota para todos os get, post, put, delete...(MainURL)
-
#### Router.JS
- [ ] Todas as rotas devem estar direcionando a um ponto existente.
-
#### Futuros planos:
- [ ] Usuario Logar/Deslogar
- [ ] Set Email Usuário
- [ ] Set Senha Usuário
- [ ] Painel de Login
- [ ] Página Principal conforme design
- [ ] Utilizar logo empresa na Dash
- [ ] Configurar as rotas do estilo parada tipo: '/listarViagem/:id/paradas' utilizando o id no edit;
-
#### Futura pag TripEmail:
- [ ] Implementar sistemas de envio de email para multiplos clientes.
- [ ] Permitir Admin adicionar usuario/email para envio.
-
#### Quando tudo estiver pronto:
- [ ] Otimizar código
- [ ] Atualizar icon 24x24
- [ ] Criar Banner
- [ ] Melhorar Design da página
- [ ] Configurar Comentários para fotos/Pagina